const Docker = require('dockerode');
const docker = new Docker({socketPath: '/var/run/docker.sock'});

const games = {
  ark: 'def1ce0fca5d',
}

module.exports = (game) => {
  if(!Object.keys(games).includes(game));
  const container = docker.getContainer(games.game);
  
  this.status = () => {
    container.inspect((error,data) => {
      if (error) throw error;
      return data;
    });
  }

  this.start = () => {
    container.start();
  };

  return this;

}