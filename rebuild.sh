#!/bin/bash

git pull
docker build -t kolson22/mike-bot .
docker stop mike-bot
docker rm mike-bot
docker run -d --name mike-bot -v /var/run/docker.sock:/var/run/docker.sock kolson22/mike-bot
